﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class bombCooldown : MonoBehaviour
{
    public bool bombReady = true;
    public float cooldownTime = 10f;
    public int currentState = 14;
    public int maxFrames = 14;

    [SerializeField] Sprite[] bombStates;

    public void DoCountdown()
    {
        if (currentState <= 14 && !bombReady)
        {
            InvokeRepeating("moveBombIndex",  0f, cooldownTime / maxFrames);
        }
    }

    void ResetBomb()
    {
        currentState = 0;
    }

    void moveBombIndex()
    {
        currentState++;         

        if (currentState <= (maxFrames - 1))
        {
            GetComponent<Image>().sprite = bombStates[currentState];
        }
        else if (currentState == maxFrames)
        {
            bombReady = true;
        }

    }
}
