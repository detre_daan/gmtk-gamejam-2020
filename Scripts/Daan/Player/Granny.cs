﻿using System.Collections;
using System.Collections.Generic;
using System.Xml;
using UnityEngine;
using UnityEngine.UI;
using UnityEngineInternal;

public class Granny : MonoBehaviour
{
    [SerializeField] float maxMouseDistance = 5f;
    [SerializeField] float moveSpeed = 1f;
    [SerializeField] GameObject water = null;
    [SerializeField] float waterDisapearMain = 2f;
    [SerializeField] float waterLandDelay = 0.3f;
    [SerializeField] float waterSprayCooldownMain = 0.05f;
    [SerializeField] float minShoot = 2f;
    [SerializeField] bool moving = true;
    [SerializeField] GameObject bomb = null;
    [SerializeField] GameObject grannySprite;

    [Header("water")]
    [SerializeField] float waterAmountMain = 20f;
    [SerializeField] float waterAmount = 20f;
    [SerializeField] float rechargeSpeed = 2f;
    [SerializeField] Slider waterIndicator = null;

    bool isColliding;

    [Header("bomb")]

    Animator anim;
    bombCooldown bombCooldown;
    GameManager gameManager;
    Rigidbody rb;

    float waterSprayCooldown = 0;

    private void Start()
    {
        waterIndicator.maxValue = waterAmountMain;
        waterSprayCooldown = waterSprayCooldownMain;

        anim = GetComponentInChildren<Animator>();

        bombCooldown = FindObjectOfType<bombCooldown>().GetComponent<bombCooldown>();

        rb = GetComponent<Rigidbody>();
        gameManager = FindObjectOfType<GameManager>().GetComponent<GameManager>();

    }

    private void Update()
    {
        waterIndicator.value = waterAmount;

        if (!gameManager.gameHalt)
        {
            Vector2 targetPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);

            ChangeGrannyDirection(targetPos);

            if (moving)
            {
                MoveTowardsMous(targetPos);
                if (waterAmount > waterAmountMain)
                {
                    waterAmount = waterAmountMain;
                }
                else if(waterAmount != waterAmountMain)
                {
                    waterAmount += rechargeSpeed  * Time.deltaTime;
                }
                anim.SetBool("shooting", false);
            }
            else
            {
                anim.SetBool("shooting", true);

            }

            InstantiateWaterAtMousePos(targetPos);
            waterSprayCooldown -= Time.deltaTime;
        }


    }

    private void FreezeMomentum()
    {
        rb.velocity = Vector3.zero;
        rb.angularVelocity = Vector3.zero;

    }

    private void OnCollisionEnter(Collision collision)
    {
        isColliding = true;
    }

    private void OnCollisionStay(Collision collision)
    {
        isColliding = true;
    }

    private void OnCollisionExit(Collision collision)
    {
        isColliding = false;
    }

    private void ChangeGrannyDirection(Vector2 targetPos)
    {
        if (targetPos.x < 0)
        {
            Quaternion quant = new Quaternion();
            quant.Set(0, 180, 0, 1);
            grannySprite.transform.rotation = quant;
        }
        else if (targetPos.x > 0)
        {
            Quaternion quant = new Quaternion();
            quant.Set(0, 0, 0, 1);
            grannySprite.transform.rotation = quant;
        }
    }

    private void MoveTowardsMous(Vector2 targetPos)
    {
        if (Vector2.Distance(transform.position, targetPos) > maxMouseDistance)
        {
            Vector3 targetPosition = Vector3.MoveTowards(transform.position, 
                targetPos, moveSpeed * Time.fixedDeltaTime);
            rb.MovePosition(targetPosition);
            ChangeGrannyDirection(targetPos);
        }        
    }

    private void InstantiateWaterAtMousePos(Vector2 targetPos)
    {
        float distance = Vector2.Distance(targetPos, transform.position);
        if (Input.GetButton("Fire1") && waterSprayCooldown < 0 
            && distance > minShoot && waterAmount > 0)
        {
            
            waterAmount--;
            GameObject waterObject = Instantiate(water, targetPos, Quaternion.identity);
            Destroy(waterObject, waterDisapearMain);

            waterSprayCooldown = waterSprayCooldownMain;
            moving = false;
        }
        else if (Input.GetButtonDown("Fire2") && bombCooldown.bombReady)
        {
            bombCooldown.bombReady = false;
            Instantiate(bomb, transform.position, Quaternion.identity);
        }
        else if (Input.GetButtonUp("Fire1"))
        {
            moving = true;
        }
    }
}
