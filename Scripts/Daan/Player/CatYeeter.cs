﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CatYeeter : MonoBehaviour
{
    [Header("Yeet cats away")]
    [SerializeField] float yeetForce = 5f;
    [SerializeField] float yeetRadius = 2f;    
    [SerializeField] float oneShotYeetForce = 3f;
    [SerializeField] float oneShotYeetRadius = 4f;


    [SerializeField] bool yeetOneshot = false;


    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.GetComponent<Cat>())
        {
            if (!yeetOneshot)
            {
                Rigidbody catRb = other.gameObject.GetComponent<Rigidbody>();
                catRb.AddExplosionForce(yeetForce, transform.position, yeetRadius, 0, ForceMode.Impulse);
            }

        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<Cat>())
        {
            if (yeetOneshot)
            {
                Rigidbody catRb = other.gameObject.GetComponent<Rigidbody>();
                catRb.AddExplosionForce(oneShotYeetForce, transform.position, oneShotYeetRadius, 0, ForceMode.Impulse);
            }

        }
    }
}
