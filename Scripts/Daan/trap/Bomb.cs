﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bomb : MonoBehaviour
{
    [SerializeField] float attractorRadius = 25f;
    GameObject bombCooldownObject;
    [SerializeField] bool lureOn = false;
    [SerializeField] int bombLifetime = 10;
    [SerializeField] AudioClip boom;

    bombCooldown bombCooldown;

    private void Start()
    {
        bombCooldown = FindObjectOfType<bombCooldown>().GetComponent<bombCooldown>();
        GetComponent<Attractor>().attractRadius = 0;
    }

    void PlayBoom()
    {
        AudioSource.PlayClipAtPoint(boom, Camera.main.transform.position);
    }

    private void Update()
    {
        if (lureOn)
        {
            GetComponent<Attractor>().attractRadius = attractorRadius;
        }
    }

    void SetLureOn()
    {
        lureOn = true;
        StartCoroutine(EndBomb());
    }

    IEnumerator EndBomb()
    {
        yield return new WaitForSeconds(bombLifetime);
        bombCooldown.bombReady = false;
        bombCooldown.currentState = 0;
        GetComponent<Animator>().SetTrigger("endAttraction");
        bombCooldown.DoCountdown();
        Destroy(gameObject, 0.5f);
    }

    void Destroy()
    {
        Destroy(gameObject, 0.2f);
    }

}
