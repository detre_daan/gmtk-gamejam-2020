﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngineInternal;
using UnityEngine.UI;

public class Trap : MonoBehaviour
{
    public int caughtCats = 0;

    [Range(0, 1)]
    [SerializeField] float volume = 0.3f;

    [SerializeField] int catsBeenInside = 0;
    [SerializeField] float timeTrapped = 2f;
    [SerializeField] Transform catKeeper = null;
    [SerializeField] float timerMain = 5f;
    [SerializeField] Slider slider;
    [SerializeField] AudioClip catRun;
    [SerializeField] AudioClip catIn;
    [SerializeField] float maxDistanceFromGrannyInPick;

    List<GameObject> listCurrentCats = new List<GameObject>();
    GameManager gameManager;
    Granny granny;
    AudioSource audioSource;

    float timer = 0;

    private void Start()
    {
        audioSource = GetComponent<AudioSource>();
        granny = FindObjectOfType<Granny>();
        timer = timerMain;
        gameManager = FindObjectOfType<GameManager>().GetComponent<GameManager>();

        //slider.maxValue = timerMain;
    }

    private void SetSlider()
    {
        slider.value = timer;
    }

    private void Update()
    {
        //TrapMechanic();
    }

    private void TrapMechanic()
    {
        timer -= Time.deltaTime;

        if (timer <= 0 && !gameManager.gameHalt)
        {
            foreach (GameObject item in listCurrentCats)
            {
                Vector3 loc = new Vector3(Random.Range(-9, 9), Random.Range(-5, 5), 0);
                item.transform.position = loc;
            }
            caughtCats = 0;
            timer = timerMain;
        }

        SetSlider();
    }

    private void OnTriggerEnter(Collider collision)
    {
            if (collision.gameObject.GetComponent<Cat>())
            {
                audioSource.clip = catIn;
                audioSource.volume = volume;
                audioSource.Play();
                listCurrentCats.Add(collision.gameObject);
                caughtCats++;
            }
            else if (collision.gameObject.GetComponent<Granny>())
            {
                timer = timerMain;
            }
    }

    private void OnTriggerExit(Collider collision)
    {

            if (collision.gameObject.GetComponent<Cat>())
            {
                audioSource.clip = catRun;
                audioSource.volume = volume;
                audioSource.Play();
                listCurrentCats.Remove(collision.gameObject);
            if (!gameEnd)
                {                   
                    caughtCats--;
                }
            }

    }
    bool gameEnd = false;

    public void MoveAllCatsAway()
    {
        if (!gameEnd)
        {
            gameEnd = true;
            Debug.Log(listCurrentCats);
            foreach (GameObject item in listCurrentCats)
            {
                item.transform.position = new Vector2(catKeeper.position.x + Random.Range(-20, 20), catKeeper.position.y + Random.Range(-12.5f, 12.5f));
                item.GetComponent<Cat>().heading = Vector3.down;
                item.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
            }
        }

    }
}
