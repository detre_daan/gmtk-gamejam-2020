﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour
{
    [SerializeField] GameObject gameCanvas = null;
    [SerializeField] float sceneDelay = 2.5f;
    [SerializeField] bool isMenu = false;
    [SerializeField] GameObject image = null;

    public void LoadNextScene()
    {
        if (gameCanvas != null)
        {
            gameCanvas.GetComponent<Animator>().SetTrigger("fade");
        }
        if (isMenu)
        {
            gameCanvas.SetActive(false);
            image.SetActive(true);
        }

        StartCoroutine(SceneLoadDelay());
    }

    IEnumerator SceneLoadDelay()
    {
        yield return new WaitForSeconds(sceneDelay);
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    public void LoadCredits()
    {
        SceneManager.LoadScene("Credits");
    }

    public void LoadMenu()
    {
        SceneManager.LoadScene("Menu");
    }

    public void Quit()
    {
        Application.Quit();
    }

    public void Restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    private void Update()
    {
        if (SceneManager.GetActiveScene().buildIndex == 1 && Input.GetKey(KeyCode.LeftControl))
        {

        }
    }
}
