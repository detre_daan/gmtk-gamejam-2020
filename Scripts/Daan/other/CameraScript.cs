﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraScript : MonoBehaviour
{

    AudioSource audioSource;
    [SerializeField] AudioClip end;

    private void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }


    public void SetMusicVolume(float vol)
    {
        audioSource.volume = vol;
    }

    public void ChangeSong()
    {
        audioSource.clip = end;
        audioSource.Play();
    }
}
