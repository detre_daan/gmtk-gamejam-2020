﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterSFX : MonoBehaviour
{
    [Range(0, 1)]
    [SerializeField] float volume = 0.75f;
    [SerializeField] AudioClip waterSplash;
    AudioSource source;

    private void Start()
    {
        source = GetComponent<AudioSource>();
    }
    public void PlayClip()
    {
        source.clip = waterSplash;
        source.volume = volume;
        source.Play();
    }
}
