﻿using Cinemachine;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public bool gameHalt = false;
    public int caughtCats = 0;
    public float gameTime = 90F;

    [SerializeField] GameObject endPos = null;
    [SerializeField] GameObject cam = null;

    [Header("GameCanvas")]
    [SerializeField] GameObject gameCanvas = null;
    [SerializeField] TextMeshProUGUI timeText = null;
    [SerializeField] TextMeshProUGUI catchText = null;

    [Header("EndCanvas")]
    [SerializeField] GameObject endCanvas = null;
    [SerializeField] TextMeshProUGUI endText = null;
    [SerializeField] TextMeshProUGUI endCaughCatsText = null;
    [SerializeField] string textForAllCatsCaught = "meeeeeeeeeeeeeh";
    [SerializeField] string textForTimeUp = "meeeeeeeeeeeeeh";

    [Header("PauseCanvas")]
    [SerializeField] GameObject pauseCanvas = null;

    Trap trap;
    Granny granny;

    bool debugMode = false;
    bool pauseOpen = false;
    int spawnedCats = 0;

    private void Start()
    {
        granny = FindObjectOfType<Granny>().GetComponent<Granny>();
        trap = FindObjectOfType<Trap>().GetComponent<Trap>();
        spawnedCats = FindObjectOfType<CatManager>().GetComponent<CatManager>().catCount;
        gameCanvas.SetActive(true);
        pauseCanvas.SetActive(false);
        endCanvas.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (!debugMode)
        {
            if (!gameHalt)
            {
                gameTime -= Time.deltaTime;
                timeText.text = Mathf.Round(gameTime).ToString();
                catchText.text = trap.caughtCats.ToString();

                EndGame();
            }
                HandlePauseMenu();
        }

    }

    void HandlePauseMenu()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (!pauseOpen)
            {
                gameHalt = true;
                pauseOpen = true;
                pauseCanvas.SetActive(true);
                StartStopGame(false);
            }
            else if (pauseOpen)
            {
                gameHalt = false;
                pauseOpen = false;
                pauseCanvas.SetActive(false);
                StartStopGame(true);
            }
        }
    }

    void StartStopGame(bool start)
    {
        Rigidbody[] rbAll = Rigidbody.FindObjectsOfType(typeof(Rigidbody)) as Rigidbody[];

        if (start)
        {
            foreach (Rigidbody rigidbody in rbAll)
            {
                if (rigidbody.transform.gameObject.tag != "staticObject")
                {
                    rigidbody.isKinematic = false;
                }
            }
        }
        else if (!start)
        {
            foreach (Rigidbody rigidbody in rbAll)
            {
                rigidbody.isKinematic = true;
            }
        }
    }

    void EndGame()
    {
        if (gameTime <= 1)
        {
            endText.text = textForTimeUp;
            DoEndStuff();
        }
        else if (caughtCats == spawnedCats)
        {
            endText.text = textForAllCatsCaught;
            DoEndStuff();
        }

    }

    private void DoEndStuff()
    {
        StartCoroutine(EndStuff());
    }

    IEnumerator EndStuff()
    {        
        
        gameCanvas.GetComponent<Animator>().SetTrigger("fade");
        cam.GetComponent<CameraScript>().ChangeSong();
        yield return new WaitForSeconds(2);

        Quaternion quant = new Quaternion();
        quant.Set(0, 0, 0,1);

        //granny.transform.position = endPos.transform.position;
        granny.transform.rotation = quant;

        gameCanvas.SetActive(false);
        endCanvas.SetActive(true);
        gameHalt = true;
        caughtCats = FindObjectOfType<Trap>().GetComponent<Trap>().caughtCats;
        if (caughtCats == 1)
        {
            endCaughCatsText.text = caughtCats.ToString() + " cat!";
        }
        else
        {
            endCaughCatsText.text = caughtCats.ToString() + " cats!";
        }
        trap.MoveAllCatsAway();

        var camera = Camera.main;

        camera.GetComponent<CinemachineBrain>().enabled = false;
        camera.transform.position = new Vector3( endPos.transform.position.x + 20, endPos.transform.position.y, -10);
        camera.orthographicSize = 20;
    }
}
