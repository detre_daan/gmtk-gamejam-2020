﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CMAudio : MonoBehaviour
{
    AudioSource audioSource;
    GameManager gameManager = null;

    [Range(0, 1)]
    [SerializeField] float volume = 1;
    [SerializeField] AudioClip tikTak;

    bool tiktakOn = false;
    // Start is called before the first frame update
    void Start()
    {
        gameManager = FindObjectOfType<GameManager>().GetComponent<GameManager>();
        audioSource = GetComponent<AudioSource>();

        audioSource.clip= tikTak;
        audioSource.volume = volume;

    }

    private void Update()
    {
        if (gameManager.gameTime < 7.5 && !tiktakOn)
        {
            tiktakOn = true;
            audioSource.Play();
        }
    }
}
