﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class SetVolume : MonoBehaviour
{
    [SerializeField] AudioMixer mixer;
    [SerializeField] float startValue = 0.3f;

    private void Start()
    {
        mixer.SetFloat("MusicVol", Mathf.Log10(startValue) * 20);
    }

    public void SetAudioLevel(float sliderValue)
    {
        mixer.SetFloat("MusicVol", Mathf.Log10(sliderValue) *20);
    }
}
