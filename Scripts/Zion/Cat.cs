﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cat : MonoBehaviour {
    private CatManager manager;
    Animator anim;
    [HideInInspector]
    public Vector3 heading;
    Vector3 target, catAvoidTarget, coheseTarget, alignTarget, deflectAvoidTarget, attractTarget, buildingAvoidTarget;
    List<Cat> nearbyCats;
    List<Attractor> attractors;
    List<Deflector> deflectors;
    List<DeflectorBox> deflectorBoxes;
    Rigidbody rb;
    bool isFlipped;
    bool isColliding;
    GameManager gameManager;
    float angle;
    public Direction dir;

    public float speed, viewRange, viewAngle, avoidRange;

    [Range(0, 1)]
    public float catAvoidWeight, coheseWeight, alignWeight, deflectorAvoidWeight, attractorWeight;
    [Range(0,5)]
    public float avoidBuildingWeight;

    [Range(0, 0.2f)]
    public float turningSpeed;
    private void Start() {
        rb = GetComponent<Rigidbody>();
        manager = GameObject.FindGameObjectWithTag("CatManager").GetComponent<CatManager>();
        manager.globalCats.Add(this);
        heading = Random.insideUnitCircle.normalized;
        anim = GetComponentInChildren<Animator>();
        if (nearbyCats == null) {
            nearbyCats = new List<Cat>();
        }
        if (attractors == null) {
            attractors = new List<Attractor>();
        }
        if (deflectors == null) {
            deflectors = new List<Deflector>();
        }
        if(deflectorBoxes == null) {
            deflectorBoxes = new List<DeflectorBox>();
        }
        gameManager = FindObjectOfType<GameManager>().GetComponent<GameManager>();
    }

    private void FixedUpdate() {
        if (!gameManager.gameHalt)
        {
            alignTarget = coheseTarget = catAvoidTarget = deflectAvoidTarget = target = buildingAvoidTarget = attractTarget = Vector3.zero;
            GetNearby();
            if (nearbyCats.Count != 0 || attractors.Count != 0 || deflectors.Count != 0 || deflectorBoxes.Count != 0) {
                Separate();
                Attract();
                Deflect();
                Cohese();
                Align();
                Attract();
                AvoidBuildings();
                WeightTargets();
                heading = Vector3.Lerp(heading, target, turningSpeed).normalized;
            }
            dir = CalculateWhichDirection();
            Debug.DrawRay(transform.position, catAvoidTarget * 10, Color.blue);
            Debug.DrawRay(transform.position, alignTarget * 10, Color.magenta);
            Debug.DrawRay(transform.position, deflectAvoidTarget * 10, Color.cyan);
            Debug.DrawRay(transform.position, buildingAvoidTarget * 10, Color.green);
            Debug.DrawRay(transform.position, coheseTarget * 10, Color.white);
            Debug.DrawRay(transform.position, attractTarget * 10, Color.yellow);
            Debug.DrawRay(transform.position, heading * 10, Color.red);
            Move();
            if (!isColliding) {
                FreezeMomentum();
            }
        }

    }
    private void Move() {
        rb.MovePosition(transform.position + heading * speed * Time.fixedDeltaTime);
    }


    private void GetNearby() {
        nearbyCats.Clear();
        attractors.Clear();
        deflectors.Clear();
        deflectorBoxes.Clear();
        foreach(Cat c in manager.globalCats) {
            if(c != this) {
                if (DistanceTo(c.transform.position) < viewRange && AngleTo(c.transform.position) < viewAngle) {
                    nearbyCats.Add(c);
                }
            }
        }

        foreach(Deflector d in manager.globalDeflectors) {
            if (DistanceTo(d.transform.position) < d.deflectRadius) {
                deflectors.Add(d);
            }
        }

        foreach(DeflectorBox db in manager.globalDeflectorBoxes) {
            if (db.Contains(transform.position)) {
                deflectorBoxes.Add(db);
            }
        }
        
        foreach(Attractor a in manager.globalAttractors) {
            if (DistanceTo(a.transform.position) < a.attractRadius) {
                attractors.Add(a);
            }
        }
    }

    private void OnDestroy() {
        manager.globalCats.Remove(this);
    }
    private void OnCollisionEnter(Collision collision) {
        isColliding = true;
    }

    private void OnCollisionStay(Collision collision) {
        isColliding = true;
    }

    private void OnCollisionExit(Collision collision) {
        isColliding = false;
    }
    private void Separate() {
        catAvoidTarget = deflectAvoidTarget = Vector3.zero;
        if (nearbyCats.Count != 0) {
            foreach(Cat c in nearbyCats) {
                if (DistanceTo(c.transform.position) < avoidRange) {
                    catAvoidTarget -= c.transform.position - transform.position;
                }
                catAvoidTarget.Normalize();
            }
        }

    }
    
    private void FreezeMomentum() {
        rb.velocity = Vector3.zero;
        rb.angularVelocity = Vector3.zero;

    }

    private void Deflect() {
        if (deflectors.Count != 0) {
            foreach (Deflector d in deflectors) {
                deflectAvoidTarget -= d.transform.position - transform.position;
                deflectAvoidTarget.Normalize();
            }
        }  
    }

    private void AvoidBuildings() {
        if (deflectorBoxes.Count != 0) {
            foreach (DeflectorBox db in deflectorBoxes) {
                buildingAvoidTarget -= db.DeflectorVector(transform.position);
                buildingAvoidTarget.Normalize();
            }
        }
    }
    private void Attract() {
        if (attractors.Count == 0) {
            return;
        }
        float x, y;
        x = y = 0;
        foreach(Attractor a in attractors) {
            x += a.transform.position.x;
            y += a.transform.position.y;
        }
        x /= attractors.Count;
        y /= attractors.Count;
        attractTarget = new Vector3(x, y, 0) - transform.position;
        attractTarget.Normalize();
    }

    private void Cohese() {
        if(nearbyCats.Count == 0) {
            return;
        }

        float x, y;
        x = y = 0;
        foreach(Cat c in nearbyCats) {
            x += c.transform.position.x;
            y += c.transform.position.y;
        }

        x /= nearbyCats.Count;
        y /= nearbyCats.Count;

        coheseTarget += new Vector3(x, y, 0) - transform.position;

        coheseTarget.Normalize();
    }

    private void Align() {
        if(nearbyCats.Count == 0) {
            return;
        }

        foreach(Cat c in nearbyCats) {
            alignTarget += c.heading;
        }

        alignTarget.Normalize();
    }

    private void WeightTargets() {
        target += (buildingAvoidTarget * avoidBuildingWeight) + (coheseTarget * coheseWeight) + (alignTarget * alignWeight) + (deflectAvoidTarget * deflectorAvoidWeight) + (attractTarget * attractorWeight) + (catAvoidTarget * catAvoidWeight);
        target.Normalize();
    }

    private float DistanceTo(in Vector3 other) {
        return (other - transform.position).magnitude;
    }
    private float AngleTo(in Vector3 other) {
        angle = Vector3.SignedAngle(heading.normalized, (other - transform.position).normalized, Vector3.forward);

        return Mathf.Abs(angle);
    }

    private Direction CalculateWhichDirection() {
        angle = Vector3.SignedAngle(transform.right, heading.normalized, Vector3.forward);
        if (angle >= -45 && angle < 45) {
            return Direction.right;
        }
        else if (angle >= 45 && angle < 135) {
            return Direction.up;
        }
        else if (angle >= 135 || angle < -135) {
            return Direction.left;
        }
        else {
            return Direction.down;
        }
    }

    private void Update() {
        anim.SetInteger("Direction", (int)dir);
        if(dir == Direction.right && !isFlipped) {
            isFlipped = true;
            anim.transform.localScale = new Vector3(-anim.transform.localScale.x, anim.transform.localScale.y, anim.transform.localScale.z);
        }
        if (isFlipped && dir != Direction.right) {
            isFlipped = false;
            anim.transform.localScale = new Vector3(-anim.transform.localScale.x, anim.transform.localScale.y, anim.transform.localScale.z);
        }
    }
    public enum Direction
    {
        up,
        down,
        left,
        right
    };
}
