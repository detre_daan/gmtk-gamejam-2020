﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class Deflector : MonoBehaviour {
    public float deflectRadius;
    CatManager manager;
    
    private void Start() {
        manager = GameObject.FindGameObjectWithTag("CatManager").GetComponent<CatManager>();
        if(manager == null) {
            Debug.LogError("Deflector list in cat manager is not initialized");
        }
        if (!manager.globalDeflectors.Contains(this)) {
            manager.globalDeflectors.Add(this);
        }
    }
    private void OnDisable () {
        manager.globalDeflectors.Remove(this);
    }

    private void OnDrawGizmos() {
        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(transform.position, deflectRadius);
    }
    public void SetRadius(float newRadius) {
        if(newRadius > 0) {
            deflectRadius = newRadius;
        }
    }
}

