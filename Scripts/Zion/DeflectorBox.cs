﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SocialPlatforms;

public class DeflectorBox : MonoBehaviour
{
    [Range(.1f, 400)]
    public float sizeX, sizeY;
    private Vector2 rangeX, rangeY;
    CatManager manager;
    Collider col;
    private void Start() {
        col = GetComponent<BoxCollider>();
        manager = GameObject.FindGameObjectWithTag("CatManager").GetComponent<CatManager>();
        if (!manager.globalDeflectorBoxes.Contains(this)) {
            manager.globalDeflectorBoxes.Add(this);
        }
        rangeX = new Vector2(transform.position.x - sizeX / 2, transform.position.x + sizeX / 2);
        rangeY = new Vector2(transform.position.y - sizeY / 2, transform.position.y + sizeY / 2);
    }

    void OnDisable() {
        manager.globalDeflectorBoxes.Remove(this);
    }

    public bool Contains(in Vector3 other) {
        if (other.x < rangeX.y && other.x > rangeX.x && other.y < rangeY.y && other.y > rangeY.x) {
            return true;
        }
        else return false;

    }

    private void OnDrawGizmos() {
        Gizmos.color = Color.blue;
        Gizmos.DrawWireCube(transform.position, new Vector3(sizeX, sizeY, 1));
    }
    public Vector3 DeflectorVector(in Vector3 catPosition) {
        if (catPosition.x < col.transform.position.x + col.bounds.size.x / 2 && catPosition.x > col.transform.position.x - col.bounds.size.x / 2) {
            if (catPosition.y > transform.position.y) {
                return -transform.up;
            }
            else {
                return transform.up;
            }
        }
        else if(catPosition.y < col.transform.position.y + col.bounds.size.y / 2 && catPosition.y > col.transform.position.y - col.bounds.size.y / 2) {
            if(catPosition.x > transform.position.x) {
                return -transform.right;
            }
            else {
                return transform.right;
            }
        }
        else {
            return (transform.position - catPosition).normalized;   
        }
    }
}
