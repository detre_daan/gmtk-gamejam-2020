﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Attractor : MonoBehaviour
{
    public float attractRadius;
    CatManager manager;
    private void Start() {
        manager = GameObject.FindGameObjectWithTag("CatManager").GetComponent<CatManager>();
        if (!manager.globalAttractors.Contains(this)) {
            manager.globalAttractors.Add(this);
        }
    }

    private void OnDisable() {
        manager.globalAttractors.Remove(this);
    }
    private void OnDrawGizmos() {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, attractRadius);
    }
}
