﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CatManager : MonoBehaviour{
    public List<Cat> globalCats;
    public List<Attractor> globalAttractors;
    public List<Deflector> globalDeflectors;
    public List<DeflectorBox> globalDeflectorBoxes;
    public List<GameObject> catPrefabs;
    GameObject catPrefab;
    public int catCount;
    public float spawnSizeX, spawnSizeY;
    private bool spawnedSuccessfully;
    public int maxSearchAttempts = 50;
    private int currentSearchAttempts = 0;
    private void Start() {
        spawnedSuccessfully = false;
        Collider[] debugArray;
        for(int i = 0; i < catCount; i++) {
            int index = Random.Range(0, catPrefabs.Count);
            catPrefab = catPrefabs[index];
            while (!spawnedSuccessfully) {
                Vector3 loc = new Vector3(Random.Range(-spawnSizeX / 2, spawnSizeX / 2), Random.Range(-spawnSizeY / 2, spawnSizeY / 2), 0);
                loc += transform.position;
                debugArray = Physics.OverlapBox(loc, catPrefab.transform.lossyScale / 2);
                if(currentSearchAttempts >= maxSearchAttempts) {
                    Debug.LogError("Spawner cannot find a free location for this cat, please increase the size of the catmanager's spawn area, or reduce the number of cats");
                    spawnedSuccessfully = true;
                }
                else if (debugArray.Length != 0) {
                    spawnedSuccessfully = false;
                    currentSearchAttempts++;
                    
                }
                else{
                    Instantiate(catPrefab, loc, Quaternion.identity, transform);
                    spawnedSuccessfully = true;
                    currentSearchAttempts = 0;
                }
            }
            spawnedSuccessfully = false;
        }
    }

    private void OnDrawGizmos() {
        Gizmos.color = Color.cyan;
        Gizmos.DrawWireCube(transform.position, new Vector3(spawnSizeX, spawnSizeY, 0));
    }
}
